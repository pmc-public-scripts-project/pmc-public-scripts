Param(
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [string]$Password = "",
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$Delete = $false,
    [Parameter(ParameterSetName="Help", Mandatory=$true)]
    [switch]$Help
)

# -----------------------------------------------------------------------------
# [Paul] This is Powershell 5.0 way of determining if it is not a Windows machine

$OldIsWindows = $false
$OldIsMacOS = $false
$OldIsLinux = $false

if (!(Get-Command uname -ErrorAction SilentlyContinue)) {
    Write-Host "Detected OS: Windows"
    $OldIsWindows = $true
} else {
    $unameOutput = uname -s
    if ($unameOutput -eq "Darwin") {
    Write-Host "Detected OS: MacOS"
        $OldIsMacOS = $true
    } else {
    Write-Host "Detected OS: Linux"
        $OldIsLinux = $true
    }
}

# -----------------------------------------------------------------------------

Function Exit-Script {
    Param(
        [Parameter(Mandatory=$false)]
        [int]$errorLevel
    )

    if ($Pause) {
        Write-Host -NoNewLine 'Press any key to continue...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
    }

    Exit $errorLevel
}

# -----------------------------------------------------------------------------

if ($Help) {
    Write-Host "PowerShell PDF Decryptor"
    Write-Host ""
    Write-Host "Usages:"
    Write-Host "    ps-pdf-decrypt [-Password <Password>] [-Delete]"
    Write-Host "    ps-pdf-decrypt -Help"
    Write-Host ""
    Write-Host "Options:"
    Write-Host "    -Password <Password> Use password for decrypting (Default is will ask)"
    Write-Host "    -Delete              Deleted original file (Default is false)"
    Write-Host ""

    Exit-Script
}

# -----------------------------------------------------------------------------

if ($Password -eq "") {
    $Password = Read-Host "Please enter the password"
}

# -----------------------------------------------------------------------------
# Step: Check Directories

$inputDir = 'input'
$outputDir = 'output'
$separatorDir = '\'

# [Paul] Ternary operators is not yet available in Powershell 5.0
if ($OldIsMacOS) {
    $separatorDir = '/'
}

if (!(Test-Path $inputDir) -or !(Test-Path $outputDir)) {
    Write-Host "`input`, or `output` directory does not exist. Exiting script..."
    Exit-Script 1
}

# -----------------------------------------------------------------------------

Function Check-QPDF {
    if (!(Get-Command qpdf -ErrorAction SilentlyContinue)) {
        Write-Output "QPDF is not installed on this system."
        Exit-Script 1
    }
}

# -----------------------------------------------------------------------------
# Step: Check Tools

Check-QPDF

# -----------------------------------------------------------------------------
# Step: Conversion (Prepare)

Remove-Item -Path "${inputDir}${separatorDir}.DS_Store" -ErrorAction SilentlyContinue
Remove-Item -Path "${inputDir}${separatorDir}Thumbs.db" -ErrorAction SilentlyContinue
Remove-Item -Path "${inputDir}${separatorDir}desktop.ini" -ErrorAction SilentlyContinue

# -----------------------------------------------------------------------------
# Step: Conversion

$files = Get-ChildItem -Path $inputDir -Filter "*.pdf"

$currentDir = Get-Location
$inputDirFull = Join-Path $currentDir $inputDir
$outputDirFull = Join-Path $currentDir $outputDir

foreach ($file in $files) {
    $filename = [System.IO.Path]::GetFileNameWithoutExtension($file.FullName)
    $extension = $file.Extension
	$inputFilename =  $inputDirFull + $separatorDir + $filename
    $outputFilename = $outputDirFull + $separatorDir + $filename

    if ($OldIsWindows) {
        $command = "qpdf --% --decrypt --password=`"${Password}`" `"${inputFilename}${extension}`" `"${outputFilename}${extension}`""
    } else {
        $command = "qpdf     --decrypt --password=`"${Password}`" `"${inputFilename}${extension}`" `"${outputFilename}${extension}`""
    }
    Invoke-Expression -Command "$command"

    if ($Delete) {
        if (Test-Path -Path "${outputFilename}${extension}") {
            Remove-Item -Path "${inputFilename}${extension}" -ErrorAction SilentlyContinue
        } else {
            Write-Output "An error encountered."
            Exit-Script 1
        }
    }
}

# -----------------------------------------------------------------------------
# Step: Output

if ($OldIsMacOS) { open "$outputDir" } else { start "$outputDir" }

Exit-Script