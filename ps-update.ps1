# -----------------------------------------------------------------------------
# [Paul] This is Powershell 5.0 way of determining if it is not a Windows machine

$OldIsWindows = $false
$OldIsMacOS = $false
$OldIsLinux = $false

if (!(Get-Command uname -ErrorAction SilentlyContinue)) {
    Write-Host "Detected OS: Windows"
    $OldIsWindows = $true
} else {
    $unameOutput = uname -s
    if ($unameOutput -eq "Darwin") {
    Write-Host "Detected OS: MacOS"
        $OldIsMacOS = $true
    } else {
    Write-Host "Detected OS: Linux"
        $OldIsLinux = $true
    }
}

# -----------------------------------------------------------------------------

if ($OldIsLinux) {
    sudo apt update
    sudo apt dist-upgrade
    sudo apt autoremove
    sudo apt clean

    if (Get-Command -Name "snap" -ErrorAction SilentlyContinue) {
        sudo snap refresh
    }
}

if ($OldIsMacOS) {
    if (Get-Command -Name "brew" -ErrorAction SilentlyContinue) {
        brew upgrade
        brew upgrade --cask
        brew cleanup -s
    }

    if (Get-Command -Name "pod" -ErrorAction SilentlyContinue) {
        pod repo update
    }
}

if ($OldIsWindows) {
    if (Get-Command -Name "choco" -ErrorAction SilentlyContinue) {
        choco upgrade all
		if (!(Get-Command -Name "choco-cleaner" -ErrorAction SilentlyContinue)) {
            choco install choco-cleaner
        }
		choco-cleaner
    }
}

if (Get-Command -Name "npm" -ErrorAction SilentlyContinue) {
    npm update -g
    npm cache clean --force
}

if (Get-Command -Name "pip3" -ErrorAction SilentlyContinue) {
    pip3 freeze | %{$_.split('==')[0]} | %{pip3 install --upgrade --upgrade-strategy=eager --break-system-packages $_}
}

# if (Get-Command -Name "pip" -ErrorAction SilentlyContinue) {
#     pip freeze | %{$_.split('==')[0]} | %{pip install --upgrade --upgrade-strategy=eager --break-system-packages $_}
# }

# if (Get-Command -Name "sdkmanager" -ErrorAction SilentlyContinue) {
#     Write-Host "Setting Java Version to 8"

#     if ($OldIsWindows) {
#         $jdkDir = "C:\Program Files\Eclipse Adoptium"
#         $jdk8 = Get-ChildItem -Path $jdkDir -Filter "jdk-8*" -Directory | Select-Object -Last 1
#         $fullPath = Join-Path $jdkDir $jdk8
#         if (Test-Path $fullPath) {
#             $env:JAVA_HOME = $fullPath
#         } 
#     } else {
#         if (Get-Command -Name "jenv" -ErrorAction SilentlyContinue) {
#             jenv local 1.8
#         }
#     }

#     sdkmanager --update
# }