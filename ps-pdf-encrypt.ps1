Param(
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [string]$DPI = 300,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$Optimized = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [string]$Password,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$Delete = $false,
    [Parameter(ParameterSetName="Help", Mandatory=$true)]
    [switch]$Help
)

# -----------------------------------------------------------------------------
# [Paul] This is Powershell 5.0 way of determining if it is not a Windows machine

$OldIsWindows = $false
$OldIsMacOS = $false
$OldIsLinux = $false

if (!(Get-Command uname -ErrorAction SilentlyContinue)) {
    Write-Host "Detected OS: Windows"
    $OldIsWindows = $true
} else {
    $unameOutput = uname -s
    if ($unameOutput -eq "Darwin") {
    Write-Host "Detected OS: MacOS"
        $OldIsMacOS = $true
    } else {
    Write-Host "Detected OS: Linux"
        $OldIsLinux = $true
    }
}

# -----------------------------------------------------------------------------

Function Exit-Script {
    Param(
        [Parameter(Mandatory=$false)]
        [int]$errorLevel
    )

    if ($Pause) {
        Write-Host -NoNewLine 'Press any key to continue...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
    }

    Exit $errorLevel
}

# -----------------------------------------------------------------------------

if ($Help) {
    Write-Host "PowerShell PDF Encryptor"
    Write-Host ""
    Write-Host "Usages:"
    Write-Host "    ps-pdf-encrypt [-DPI <DPI>] [-Password <Password>] [-Optimized] [-Delete]"
    Write-Host "    ps-pdf-encrypt -Help"
    Write-Host ""
    Write-Host "Options:"
    Write-Host "    -DPI <DPI>           Sets DPI of the PDF (Default is 300)"
    Write-Host "    -Password <Password> Sets password when opening the PDF (Default is blank)"
    Write-Host "    -Optimized           Optimizes PDF (Default is false)"
    Write-Host "    -Delete              Deleted original file (Default is false)"
    Write-Host ""

    Exit-Script
}

# -----------------------------------------------------------------------------
# Step: Check Directories

$inputDir = 'input'
$outputDir = 'output'
$processingDir = 'processing'
$separatorDir = '\'

# [Paul] Ternary operators is not yet available in Powershell 5.0
if ($OldIsMacOS) {
    $separatorDir = '/'
}

if (!(Test-Path $inputDir) -or !(Test-Path $outputDir) -or !(Test-Path $processingDir)) {
    Write-Host "`input`, `processing`, or `output` directory does not exist. Exiting script..."
    Exit-Script 1
}

# -----------------------------------------------------------------------------

Function Check-ImageMagick {
    if (!(Get-Command magick -ErrorAction SilentlyContinue)) {
        Write-Output "Magick is not installed on this system."
        Exit-Script 1
    }
}

Function Check-ImageOptim {
    if (!(Get-Command /Applications/ImageOptim.app/Contents/MacOS/ImageOptim -ErrorAction SilentlyContinue)) {
        Write-Output "ImageOptim is not installed on this system."
        Exit-Script 1
    }
}

Function Check-FileOptimizer {
    if (!(Get-Command FileOptimizer64 -ErrorAction SilentlyContinue)) {
        Write-Output "FileOptimizer is not installed on this system."
        Exit-Script 1
    }
}

Function Check-IMG2PDF {
    if (!(Get-Command img2pdf -ErrorAction SilentlyContinue)) {
        Write-Output "IMG2PDF is not installed on this system."
        Exit-Script 1
    }
}

Function Check-QPDF {
    if (!(Get-Command qpdf -ErrorAction SilentlyContinue)) {
        Write-Output "QPDF is not installed on this system."
        Exit-Script 1
    }
}

# -----------------------------------------------------------------------------
# Step: Check Tools

Check-ImageMagick
Check-IMG2PDF
Check-QPDF

if ($Optimized) {
    if ($OldIsMacOS) {
        Check-ImageOptim
    } else {
        Check-FileOptimizer
    }
}

# -----------------------------------------------------------------------------
# Step: Conversion (Prepare)

Remove-Item -Path "${inputDir}${separatorDir}.DS_Store" -ErrorAction SilentlyContinue
Remove-Item -Path "${inputDir}${separatorDir}Thumbs.db" -ErrorAction SilentlyContinue
Remove-Item -Path "${inputDir}${separatorDir}desktop.ini" -ErrorAction SilentlyContinue

# -----------------------------------------------------------------------------
# Step: Conversion

Remove-Item -Path "${processingDir}${separatorDir}*" -Force -Recurse

$files = Get-ChildItem -Path $inputDir -Filter "*.pdf"

$currentDir = Get-Location
$inputDirFull = Join-Path $currentDir $inputDir
$outputDirFull = Join-Path $currentDir $outputDir

foreach ($file in $files) {
    New-Item -ItemType Directory -Path "${processingDir}${separatorDir}input" | Out-Null
    New-Item -ItemType Directory -Path "${processingDir}${separatorDir}exported" | Out-Null
    New-Item -ItemType Directory -Path "${processingDir}${separatorDir}combined" | Out-Null
    New-Item -ItemType Directory -Path "${processingDir}${separatorDir}output" | Out-Null

    $filename = [System.IO.Path]::GetFileNameWithoutExtension($file.FullName)
    $extension = $file.Extension
	$inputFilename =  $inputDirFull + $separatorDir + $filename
    $outputFilename = $outputDirFull + $separatorDir + $filename

    Write-Output "PROCESSING: ${inputFilename}${extension}"
    
    Copy-Item -Path "${inputFilename}${extension}" -Destination "${processingDir}\input"

    Write-Output "Stage 1: Exporting PDF to PNG..."
	# Add "-alpha off" if it has image
    magick -density ${DPI} -quality 100 -colorspace RGB "${processingDir}${separatorDir}input${separatorDir}${filename}${extension}" "${processingDir}${separatorDir}exported${separatorDir}%05d.png" | Out-Null

    Write-Output "Stage 2: Optimizing PNG..."
    if ($Optimized) {
        if ($OldIsMacOS) {
            Invoke-Expression -Command "/Applications/ImageOptim.app/Contents/MacOS/ImageOptim '${processingDir}${separatorDir}exported' | Out-Null"
        } else {
            Invoke-Expression -Command "FileOptimizer64 '${currentDir}${separatorDir}${processingDir}${separatorDir}exported' | Out-Null"
        }
    } else {
        Write-Output "Skipped..."
    }

    Write-Output "Stage 3: Combining PNG to PDF..."
    img2pdf --output "${processingDir}${separatorDir}combined${separatorDir}combined.pdf" ${processingDir}${separatorDir}exported${separatorDir}*.png | Out-Null

    Write-Output "Stage 4: Securing PDF..."

    $guid = [guid]::NewGuid()

    if ($OldIsWindows) {
	    $command = "qpdf --% --encrypt `"${Password}`" `"${guid}`" 256 --extract=n --modify=none --print=full -- `"${processingDir}${separatorDir}combined${separatorDir}combined.pdf`" `"${processingDir}${separatorDir}output${separatorDir}secured.pdf`" --allow-weak-crypto"
    } else {
        $command = "qpdf     --encrypt `"${Password}`" `"${guid}`" 256 --extract=n --modify=none --print=full -- `"${processingDir}${separatorDir}combined${separatorDir}combined.pdf`" `"${processingDir}${separatorDir}output${separatorDir}secured.pdf`" --allow-weak-crypto"
    }
    Invoke-Expression -Command "$command"

    Move-Item -Path "${processingDir}${separatorDir}output${separatorDir}secured.pdf" -Destination "${outputFilename}${extension}" -Force

    if ($Delete) {
        if (Test-Path -Path "${outputFilename}${extension}") {
            Remove-Item -Path "${inputFilename}${extension}" -ErrorAction SilentlyContinue
        } else {
            Write-Output "An error encountered."
            Exit-Script 1
        }
    }

    Remove-Item -Path "${processingDir}${separatorDir}*" -Force -Recurse
}

# -----------------------------------------------------------------------------
# Step: Output

if ($OldIsMacOS) { open "$outputDir" } else { start "$outputDir" }

Exit-Script