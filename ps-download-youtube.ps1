Param(
    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="List", Mandatory=$true)]
    [string]$Link = "",

    [Parameter(ParameterSetName="DownloadFile", Mandatory=$true)]
    [string]$File = "",

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [ValidateSet("av1", "mp4", "webm", "m4a", "mp3")]
    [string]$Format,

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [switch]$Best = $false,

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [string]$OutputDir = "",

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [string]$Archive = "",

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [switch]$PrefixDate = $false,

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [switch]$PrefixIndex = $false,

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [switch]$Retry = $false,

    [Parameter(ParameterSetName="DownloadLink", Mandatory=$false)]
    [Parameter(ParameterSetName="DownloadFile", Mandatory=$false)]
    [switch]$Ping = $false,

    [Parameter(ParameterSetName="List", Mandatory=$true)]
    [switch]$ListFormats = $false,

    [Parameter(ParameterSetName="Help", Mandatory=$true)]
    [switch]$Help
)

# -----------------------------------------------------------------------------
# [Paul] This is Powershell 5.0 way of determining if it is not a Windows machine

$OldIsWindows = $false
$OldIsMacOS = $false
$OldIsLinux = $false

if (!(Get-Command uname -ErrorAction SilentlyContinue)) {
    Write-Host "Detected OS: Windows"
    $OldIsWindows = $true
} else {
    $unameOutput = uname -s
    if ($unameOutput -eq "Darwin") {
    Write-Host "Detected OS: MacOS"
        $OldIsMacOS = $true
    } else {
    Write-Host "Detected OS: Linux"
        $OldIsLinux = $true
    }
}

# -----------------------------------------------------------------------------

Function Exit-Script {
    Param(
        [Parameter(Mandatory=$false)]
        [int]$errorLevel
    )

    if ($Pause) {
        Write-Host -NoNewLine 'Press any key to continue...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
    }

    Exit $errorLevel
}

# -----------------------------------------------------------------------------

if ($Help) {
    Write-Host "PowerShell YouTube Downloader"
    Write-Host ""
    Write-Host "Usages:"
    Write-Host "    ps-download-youtube [-Link <Link>] [-File <File>] [-Format <Format>] [-Best] [-OutputDir <OutputDir>] [-Archive <Archive>] [-PrefixDate] [-PrefixIndex] [-Retry] [-Ping]"
    Write-Host "    ps-download-youtube -ListFormats"
    Write-Host "    ps-download-youtube -Help"
    Write-Host ""
    Write-Host "Options:"
    Write-Host "    -Link <Link>           Either a video or playlist. (Default: It will ask on runtime.)"
    Write-Host "    -File <Link>           Download from a file."
    Write-Host "    -Format <Link>         Download in what format. (Default: mp4)"
    Write-Host "    -Best                  Download the best quality. (Default: false)"
    Write-Host "    -OutputDir <OutputDir> Set tutput directory. (Default: Current directory.)"
    Write-Host "    -Archive <Archive>     Utilize archiving (Default: Blank, it will not use archiving)"
    Write-Host "    -PrefixDate            Add upload date as prefix. (Default: false)"
    Write-Host "    -PrefixIndex           Add playlist index as prefix. (Default: false)"
    Write-Host "    -Retry                 Retry downloading until it succeeds. (Default: false)"
    Write-Host "    -Ping                  Ping internet connection before downloading. (Default: false)"
    Write-Host "    -ListFormats           List all available formats"
    Write-Host "    -Help                  Show this help"
    Write-Host ""
    Write-Host "Formats:"
    Write-Host "    av1, mp4, webm, m4a, mp3"

    Exit-Script
}

# -----------------------------------------------------------------------------
# Step: Check Parameters

if ($Link -eq "" -and $File -eq "") {
    $Link = Read-Host "Please enter the link"
    if ($Link -eq "") {
        Write-Output "Link is empty."
        Exit-Script 1
    }
}

$separatorDir = '\'

# [Paul] Ternary operators is not yet available in Powershell 5.0
if ($OldIsMacOS) {
    $separatorDir = '/'
}

if ($OutputDir -eq "") {
    $OutputDirPath = Get-Location
} else {
    if (!(Test-Path $OutputDir)) {
        Write-Host "$OutputDir directory does not exist. Exiting script..."
        Exit-Script 1
    } else {
        $OutputDirPath = Convert-Path -Path $OutputDir
    }
}

if ($File -ne "") {
    if (!(Test-Path $File)) {
        Write-Host "$File does not exist. Exiting script..."
        Exit-Script 1
    }
}

# -----------------------------------------------------------------------------

Function Check-Python3 {
    if (!(Get-Command pip3 -ErrorAction SilentlyContinue)) {
        Write-Output "Python 3 is not installed on this system."
        Exit-Script 1
    }
}

Function Check-YTDLP {
    if (!(Get-Command yt-dlp -ErrorAction SilentlyContinue)) {
        Write-Output "YT-DLP is not installed on this system."
        Exit-Script 1
    }
}

Function Check-FFMPEG {
    if (!(Get-Command ffmpeg -ErrorAction SilentlyContinue)) {
        Write-Output "FFMPEG is not installed on this system."
        Exit-Script 1
    }
}

# -----------------------------------------------------------------------------
# Step: Check / Install Tools

Check-Python3

pip3 install yt-dlp --upgrade --break-system-packages

Check-YTDLP

Check-FFMPEG

# -----------------------------------------------------------------------------
# Step (Optional): List Available Formats

if ($ListFormats) {
    if ($OldIsWindows) {
        $command = "yt-dlp --% --list-formats ${Link}"
    } else {
        $command = "yt-dlp     --list-formats ${Link}"
    }

    Invoke-Expression -Command "$command"
    Exit-Script $LASTEXITCODE
}

# -----------------------------------------------------------------------------
# Step: Process Parameters

if ($Format -eq "av1") {
    $CODEC_VIDEO_EXT="mp4"
    $CODEC_AUDIO_EXT="m4a"
    $CODEC_VIDEO_VCODEC="av01"
    $CODEC_AUDIO_ACODEC="mp4a"
} elseif ($Format -eq "webm") {
    $CODEC_VIDEO_EXT="webm"
    $CODEC_AUDIO_EXT="webm"
    $CODEC_VIDEO_VCODEC="vp09"
    $CODEC_AUDIO_ACODEC="opus"
} elseif ($Format -eq "m4a") {
    $AUDIO_ONLY="true"
    $CODEC_AUDIO_EXT="m4a"
    $CODEC_AUDIO_ACODEC="mp4a"
} else {
    $CODEC_VIDEO_EXT="mp4"
    $CODEC_AUDIO_EXT="m4a"
    $CODEC_VIDEO_VCODEC="avc1"
    $CODEC_AUDIO_ACODEC="mp4a"
}

$PARAMS_EMBED_THUMBNAIL="--embed-thumbnail"

if ($Format -eq "mp3") {
   if ($Best) {
       $PARAMS_FORMAT="--extract-audio --audio-format mp3 --audio-quality 0"
   } else {
       $PARAMS_FORMAT="--extract-audio --audio-format mp3 --audio-quality 5"
   }
   $PARAMS_ALL_SUBS=""
   $PARAMS_EMBED_SUBS=""
} elseif ($AUDIO_ONLY) {
   $PARAMS_FORMAT="--format bestaudio[ext=${CODEC_AUDIO_EXT}][acodec^=${CODEC_AUDIO_ACODEC}]"
   $PARAMS_ALL_SUBS=""
   $PARAMS_EMBED_SUBS=""
} else {
    if ($Best) {
        if ($Format -eq "") {
            $PARAMS_FORMAT="--format bestvideo+bestaudio/best"                
        } else {
            $PARAMS_FORMAT="--format bestvideo[ext=${CODEC_VIDEO_EXT}][vcodec^=${CODEC_VIDEO_VCODEC}]+bestaudio[ext=${CODEC_AUDIO_EXT}][acodec^=${CODEC_AUDIO_ACODEC}]/best"                
        }
    } else {
        $PARAMS_FORMAT="--format bestvideo[height<=720][fps<=60][ext=${CODEC_VIDEO_EXT}][vcodec^=${CODEC_VIDEO_VCODEC}]+bestaudio[ext=${CODEC_AUDIO_EXT}][acodec^=${CODEC_AUDIO_ACODEC}]/bestvideo[height<=1080][fps<=30][ext=${CODEC_VIDEO_EXT}][vcodec^=${CODEC_VIDEO_VCODEC}]+bestaudio[ext=${CODEC_AUDIO_EXT}][acodec^=${CODEC_AUDIO_ACODEC}]/best"
    }

    $PARAMS_ALL_SUBS="--all-subs"
    $PARAMS_EMBED_SUBS="--embed-subs"
}

if ($Archive -eq "") {
    $PARAMS_ARCHIVE=""
} else {
    $PARAMS_ARCHIVE="--download-archive " + $OutputDirPath.ToString() + $separatorDir + $Archive
}

if ($PrefixDate) {
    $PARAMS_FILENAME="%(upload_date)s - %(title)s.%(ext)s"
} else {
    $PARAMS_FILENAME="%(title)s.%(ext)s"
}

if ($PrefixIndex) {
    $PARAMS_OUTPUT=$OutputDirPath.ToString() + $separatorDir + "%(playlist)s/%(playlist_index)s - " + $PARAMS_FILENAME
} else {
    $PARAMS_OUTPUT=$OutputDirPath.ToString() + $separatorDir + $PARAMS_FILENAME
}


# -----------------------------------------------------------------------------
# Step: Download

if ($Link -ne "") {
    $PARAMS_DOWNLOAD="${Link}"
} else {
    $PARAMS_DOWNLOAD="--batch-file ${File}"
}

if ($OldIsWindows) {
    $command = "yt-dlp --% ${PARAMS_FORMAT} ${PARAMS_ALL_SUBS} ${PARAMS_EMBED_SUBS} ${PARAMS_EMBED_THUMBNAIL} ${PARAMS_ARCHIVE} --add-metadata --no-check-certificate --skip-unavailable-fragments --ignore-errors --no-overwrites --verbose --output `"${PARAMS_OUTPUT}`" ${PARAMS_DOWNLOAD}"
} else {
    $command = "yt-dlp     ${PARAMS_FORMAT} ${PARAMS_ALL_SUBS} ${PARAMS_EMBED_SUBS} ${PARAMS_EMBED_THUMBNAIL} ${PARAMS_ARCHIVE} --add-metadata --no-check-certificate --skip-unavailable-fragments --ignore-errors --no-overwrites --verbose --output `"${PARAMS_OUTPUT}`" ${PARAMS_DOWNLOAD}"
}

do {
    if ($Ping) {
        Test-Connection -ComputerName google.com
    }

    Write-Output ""
    Write-Output "COMMAND: $command"
    Write-Output ""

    Invoke-Expression -Command "$command"
    $EXIT_CODE=$LASTEXITCODE

    if ($Retry -and $EXIT_CODE -ne 0) {
        Write-Output ""
        Write-Output "Script encountered an error, restarting..."
        Write-Output ""
        Start-Sleep -Seconds 3
    }
} while ($Retry -and $EXIT_CODE -ne 0)

# -----------------------------------------------------------------------------
# Step: Output / Exit

if ($EXIT_CODE -eq 0) {
    if ($OldIsMacOS) { open "$OutputDirPath" } else { start "$OutputDirPath" }
    Exit-Script
} else {
    Write-Output "Script encountered an error."
    Exit-Script 1
}
