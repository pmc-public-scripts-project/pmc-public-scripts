Param(
    [Parameter(ParameterSetName="Convert", Mandatory=$true)]
    [ValidateSet("png", "jpg", "webp", "avif", "grayscale", "h264", "av1", "mjpeg")]
    [string]$Format,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [ValidateSet("1080p", "fwxga", "720p", "480p", "360p", "240p", "144p", "svga", "qvga")]
    [string]$Resolution,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$NoAudio = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$NoSubtitle = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$NoAccel = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$Pause = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$Optimized = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$BestQuality = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$LowQuality = $false,
    [Parameter(ParameterSetName="Convert", Mandatory=$false)]
    [switch]$HDR = $false,
    [Parameter(ParameterSetName="Help", Mandatory=$true)]
    [switch]$Help
)

# -----------------------------------------------------------------------------
# [Paul] This is Powershell 5.0 way of determining if it is not a Windows machine

$OldIsWindows = $false
$OldIsMacOS = $false
$OldIsLinux = $false

if (!(Get-Command uname -ErrorAction SilentlyContinue)) {
    Write-Host "Detected OS: Windows"
    $OldIsWindows = $true
} else {
    $unameOutput = uname -s
    if ($unameOutput -eq "Darwin") {
    Write-Host "Detected OS: MacOS"
        $OldIsMacOS = $true
    } else {
    Write-Host "Detected OS: Linux"
        $OldIsLinux = $true
    }
}

# -----------------------------------------------------------------------------

Function Exit-Script {
    Param(
        [Parameter(Mandatory=$false)]
        [int]$errorLevel
    )

    if ($Pause) {
        Write-Host -NoNewLine 'Press any key to continue...';
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
    }

    Exit $errorLevel
}

# -----------------------------------------------------------------------------

if ($Help) {
    Write-Host "PowerShell Converter"
    Write-Host ""
    Write-Host "Usages:"
    Write-Host "    ps-convert -Format <format> [-Resolution <resolution>] [-NoAudio] [-NoSubtitle] [-NoAccel] [-Pause] [-Optimized] [[-BestQuality] | [-LowQuality]]"
    Write-Host "    ps-convert -Help"
    Write-Host ""
    Write-Host "Formats:"
    Write-Host "    png"
    Write-Host "    jpg"
    Write-Host "    webp"
    Write-Host "    avif"
    Write-Host "    grayscale"
    Write-Host "    h264"
    Write-Host "    av1"
    Write-Host "    mjpeg"
    Write-Host ""
    Write-Host "Options:"
    Write-Host "    -Resolution <resolution> Sets the resolution"
    Write-Host "    -NoAudio                 Removes audio"
    Write-Host "    -NoSubtitle              Removes subtitle"
    Write-Host "    -NoAccel                 Disables hardware acceleration"
    Write-Host "    -Pause                   Add pause at the end of conversion"
    Write-Host "    -Optimized               Prioritizes small file size"
    Write-Host "    -BestQuality             Produce near lossless video quality"
    Write-Host "    -LowQuality              Produce low quality video"
    Write-Host "    -HDR                     Converts HDR to SDR"
    Write-Host ""
    Write-Host "Resolutions:"
    Write-Host "    1080p, fwxga, 720p, 480p, 360p, 240p, 144p, svga, qvga"
    Write-Host ""

    Exit-Script
}

# -----------------------------------------------------------------------------

$inputDir = 'input'
$outputDir = 'output'
$separatorDir = '\'

# [Paul] Ternary operators is not yet available in Powershell 5.0
if ($OldIsMacOS) {
    $separatorDir = '/'
}

if (!(Test-Path $inputDir) -or !(Test-Path $outputDir)) {
    Write-Host "`input` or `output` directory does not exist. Exiting script..."
    Exit-Script 1
}

# -----------------------------------------------------------------------------

$width = 0
$height = 0

if ($Resolution -eq "1080p") {
    $width = 1920
    $height = 1080
}

if ($Resolution -eq "fwxga") {
    $width = 1366
    $height = 768
}

if ($Resolution -eq "720p") {
    $width = 1280
    $height = 720
}

if ($Resolution -eq "480p") {
    $width = 854
    $height = 480
}

if ($Resolution -eq "360p") {
    $width = 640
    $height = 360
}

if ($Resolution -eq "240p") {
    $width = 426
    $height = 240
}

if ($Resolution -eq "144p") {
    $width = 256
    $height = 144
}

if ($Resolution -eq "svga") {
    $width = 800
    $height = 600
}

if ($Resolution -eq "qvga") {
    $width = 320
    $height = 240
}

$fpsmaxParams = "-fpsmax 30"
if ($BestQuality) {
    $fpsmaxParams = ""
} elseif ($LowQuality) {
    $fpsmaxParams = "-fpsmax 24"
}

# -----------------------------------------------------------------------------

Function Is-Image {
    Param(
        [string]$Format
    )

    $imageFormats = @("grayscale", "png", "jpg", "webp", "avif")
    return $imageFormats -contains $Format.ToLower()
}

Function Check-ImageMagick {
    if (!(Get-Command magick -ErrorAction SilentlyContinue)) {
        Write-Output "Magick is not installed on this system."
        Exit-Script 1
    }
}

Function Check-FFMPEG {
    if (!(Get-Command ffmpeg -ErrorAction SilentlyContinue)) {
        Write-Output "FFMPEG is not installed on this system."
        Exit-Script 1
    }
}

Function Check-ImageOptim {
    if (!(Get-Command /Applications/ImageOptim.app/Contents/MacOS/ImageOptim -ErrorAction SilentlyContinue)) {
        Write-Output "ImageOptim is not installed on this system."
        Exit-Script 1
    }
}

Function Check-FileOptimizer {
    if (!(Get-Command FileOptimizer64 -ErrorAction SilentlyContinue)) {
        Write-Output "FileOptimizer is not installed on this system."
        Exit-Script 1
    }
}

# -----------------------------------------------------------------------------

Function Convert-Picture {
    Param(
        [Parameter(Mandatory=$true)]
        [string]$inputFile,
        [Parameter(Mandatory=$true)]
        [string]$outputFile
    )

    $tempOutputExtension = [System.IO.Path]::GetExtension($outputFile)

    $quality = "100"
    if ($LowQuality) {
        $quality = "50"
    }

    $resizeParams = ""
    if (($width -gt 0) -and ($height -gt 0)) {
        $resizeParams = "-resize '${width}X${height}>'"
    }

    $pdfParams = ""
    if ($outputExtension -eq ".pdf") {
        $pdfDensity = "600"
        if ($BestQuality) {
            $pdfDensity = "1200"
        } elseif ($LowQuality) {
            $pdfDensity = "300"
        }

        # "-alpha off" might be needed
        $pdfParams = "-density ${pdfDensity} -colorspace RGB"
        $outputFile = "${outputDirFull}${separatorDir}${outputFilename}-%04d${tempOutputExtension}"
    }

    Write-Output "magick $pdfParams -quality ${quality} '$inputFile' $resizeParams '$outputFile'"
    Invoke-Expression -Command "magick $pdfParams -quality ${quality} '$inputFile' $resizeParams '$outputFile'"
}

Function Convert-GrayScale {
	Param(
		[Parameter(Mandatory=$true)]
		[string]$inputFile,
		[Parameter(Mandatory=$true)]
		[string]$outputFile
	)
    Invoke-Expression -Command "magick '$inputFile' -colorspace Gray -quality 100 '$outputFile'"
}

Function Convert-H264 {
    Param(
        [Parameter(Mandatory=$true)]
        [string]$inputFile,
        [Parameter(Mandatory=$true)]
        [string]$outputFile
    )

    if ($HDR) {
        $videoFilters = "zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,format=yuv420p"
    } else {
        $videoFilters = ""        
    }
    
    if (($width -gt 0) -and ($height -gt 0)) {
        $videoFilters = "scale=w=${width}:h=${height}:force_original_aspect_ratio=decrease:force_divisible_by=2," + $videoFilters
    }

    $qualityBase50 = "17"
    $qualityBase100 = "66"

    if ($BestQuality) {
        $qualityBase50 = "1"
        $qualityBase100 = "100"
    } elseif ($LowQuality) {
        $qualityBase50 = "34"
        $qualityBase100 = "33"
    }

    $presetNvenc = "fast"
    $presetQsv = "veryfast"
    $presetDefault = "ultrafast"

    if ($Optimized) {
        $presetNvenc = "slow"
        $presetQsv = "veryslow"
        $presetDefault = "veryslow"        
    }

    $audioMapping = ""
    $audioParams = ""
    
    if (!$NoAudio) {
        $audioStream = $(ffprobe "$inputFile" -v error -select_streams a -show_entries stream=index:stream_tags=language -of compact=print_section=0:nokey=1:item_sep=',' | grep 'eng' | head -n 1 | cut -d ',' -f 1)

        if ($audioStream -eq $null) {
            $audioMapping = "-map 0:a:0"
        } else {
            $audioMapping = "-map 0:$audioStream"            
        }

        $audioChannels = "-ac 2"
        if ($BestQuality) {
            $audioChannels = ""
        } elseif ($LowQuality) {
            $audioChannels = "-ac 1"
        }
        
        $audioParams = "-c:a aac $audioChannels -b:a 320k -ar 48000 -strict experimental"
    }

    $subtitleMapping = ""
    $subtitleParams = ""
    $subtitleStream = $(ffprobe "$inputFile" -v error -select_streams s -show_entries stream=index:stream_tags=language -of compact=print_section=0:nokey=1:item_sep=',' | grep 'eng' | head -n 1 | cut -d ',' -f 1)

    if (!$NoSubtitle -and $subtitleStream -ne "") {
        $subtitleMapping = "-map 0:$subtitleStream"
        $subtitleParams = "-c:s mov_text -disposition:$subtitleStream forced"
    }

    if (!$NoAccel) {
        ffmpeg -loglevel error -f lavfi -i color=black:s=1080x1080 -vframes 1 -an -c:v h264_videotoolbox -f null -
        if ($LASTEXITCODE -eq 0) {
            Write-Output "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -hwaccel videotoolbox -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v h264_videotoolbox -profile:v baseline -q:v $qualityBase100 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            Invoke-Expression -Command "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -hwaccel videotoolbox -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v h264_videotoolbox -profile:v baseline -q:v $qualityBase100 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            return
        }

        ffmpeg -loglevel error -f lavfi -i color=black:s=1080x1080 -vframes 1 -an -c:v h264_nvenc -f null -
        if ($LASTEXITCODE -eq 0) {
            Write-Output "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -hwaccel cuvid -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v cuda -profile:v baseline -preset $presetNvenc -qp $qualityBase50 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            Invoke-Expression -Command "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -hwaccel cuvid -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v cuda -profile:v baseline -preset $presetNvenc -qp $qualityBase50 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            return
        }

        ffmpeg -loglevel error -f lavfi -i color=black:s=1080x1080 -vframes 1 -an -c:v h264_qsv -f null -
        if ($LASTEXITCODE -eq 0) {
            Write-Output "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -hwaccel qsv -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v h264_qsv -profile:v baseline -preset $presetQsv -global_quality $qualityBase50 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            Invoke-Expression -Command "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -hwaccel qsv -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v h264_qsv -profile:v baseline -preset $presetQsv -global_quality $qualityBase50 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            return
        }

        ffmpeg -loglevel error -f lavfi -i color=black:s=1080x1080 -vframes 1 -an -c:v h264_amf -f null -
        if ($LASTEXITCODE -eq 0) {
            Write-Output "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v h264_amf -profile:v constrained_baseline -rc cqp -qp_i $qualityBase50 -qp_p $qualityBase50 -qp_b $qualityBase50 -quality speed $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            Invoke-Expression -Command "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v h264_amf -profile:v constrained_baseline -rc cqp -qp_i $qualityBase50 -qp_p $qualityBase50 -qp_b $qualityBase50 -quality speed $audioParams $subtitleParams -movflags +faststart '$outputFile'"
            return
        }
    }
    
    Write-Output "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v libx264 -profile:v baseline -preset $presetDefault -crf $qualityBase50 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
    Invoke-Expression -Command "ffmpeg -analyzeduration 100M -probesize 50M -y -threads 0 -thread_queue_size 512 -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 $subtitleMapping -map_metadata 0 -vf '$videoFilters' -c:v libx264 -profile:v baseline -preset $presetDefault -crf $qualityBase50 $audioParams $subtitleParams -movflags +faststart '$outputFile'"
}

# A quick, low-res conversion (480p) to AV1 designed for slow internet android to facebook transfer
Function Convert-AV1 {
    Param(
        [Parameter(Mandatory=$true)]
        [string]$inputFile,
        [Parameter(Mandatory=$true)]
        [string]$outputFile
    )
    Invoke-Expression -Command "ffmpeg -y -threads 0 -thread_queue_size 512 -i '$inputFile' -map 0:a:0 -map 0:v:0 -map_metadata -1 -vf 'scale=w=854:h=480:force_original_aspect_ratio=decrease:force_divisible_by=2' -fpsmax 24 -c:a aac -strict experimental -c:v libaom-av1 -crf 34 -cpu-used 8 '$outputFile'"
}

# A best conversion to Epson S31 projector
Function Convert-MJPEG {
    Param(
        [Parameter(Mandatory=$true)]
        [string]$inputFile,
        [Parameter(Mandatory=$true)]
        [string]$outputFile
    )

    $resolutionParams = ""
    if (($width -gt 0) -and ($height -gt 0)) {
        $resolutionParams = "-vf 'scale=w=${width}:h=${height}:force_original_aspect_ratio=decrease:force_divisible_by=2'"
    }

    $qualityBase30 = "11"

    if ($BestQuality) {
        $qualityBase30 = "1"
    } elseif ($LowQuality) {
        $qualityBase30 = "22"
    }

    $audioMapping = ""
    $audioParams = ""

    if (!$NoAudio) {
        $audioMapping = "-map 0:a:0"
        $audioParams = "-c:a pcm_s16le -q:a 1 -ac 1"
    }

    Invoke-Expression -Command "ffmpeg -y -threads 0 -thread_queue_size 512 -i '$inputFile' $fpsmaxParams $audioMapping -map 0:v:0 -map_metadata -1 $resolutionParams -c:v mjpeg -q:v $qualityBase30 $audioParams '$outputFile'"
}

# -----------------------------------------------------------------------------
# Step: Check Tools

if ($Format -eq "png") { Check-ImageMagick }
if ($Format -eq "jpg") { Check-ImageMagick }
if ($Format -eq "avif") { Check-ImageMagick }
if ($Format -eq "grayscale") { Check-ImageMagick }
if ($Format -eq "h264") { Check-FFMPEG }
if ($Format -eq "av1") { Check-FFMPEG }
if ($Format -eq "mjpeg") { Check-FFMPEG }

if ($Optimized) {
    if (Is-Image $Format) {
        if ($OldIsMacOS) {
            Check-ImageOptim
        } else {
            Check-FileOptimizer
        }
    }
}

# -----------------------------------------------------------------------------
# Step: Conversion (Prepare)

Remove-Item -Path "${inputDir}${separatorDir}.DS_Store" -ErrorAction SilentlyContinue
Remove-Item -Path "${inputDir}${separatorDir}Thumbs.db" -ErrorAction SilentlyContinue
Remove-Item -Path "${inputDir}${separatorDir}desktop.ini" -ErrorAction SilentlyContinue

# -----------------------------------------------------------------------------
# Step: Conversion

$files = Get-ChildItem -Path $inputDir -File

$currentDir = Get-Location
$outputDirFull = Join-Path $currentDir $outputDir

foreach ($file in $files) {
    $outputFilename = [System.IO.Path]::GetFileNameWithoutExtension($file.FullName)
    $outputExtension = $file.Extension
    $outputFile = $outputDirFull + $separatorDir + $outputFilename

    if ($Format -eq "png") { Convert-Picture $file.FullName "$outputFile.png" }
    if ($Format -eq "jpg") { Convert-Picture $file.FullName "$outputFile.jpg" }
    if ($Format -eq "webp") { Convert-Picture $file.FullName "$outputFile.webp" }
    if ($Format -eq "avif") { Convert-Picture $file.FullName "$outputFile.avif" }
    if ($Format -eq "grayscale") { Convert-GrayScale $file.FullName "$outputFile$outputExtension" }
    if ($Format -eq "h264") { Convert-H264 $file.FullName "$outputFile.mp4" }
    if ($Format -eq "av1") { Convert-AV1 $file.FullName "$outputFile.mp4" }
    if ($Format -eq "mjpeg") { Convert-MJPEG $file.FullName "$outputFile.avi" }
}

# -----------------------------------------------------------------------------
# Step: Optimization

if ($Optimized) {
    if (Is-Image $Format) {
        if ($OldIsMacOS) {
            Invoke-Expression -Command "/Applications/ImageOptim.app/Contents/MacOS/ImageOptim '$outputDirFull'"
        } else {
            Invoke-Expression -Command "FileOptimizer64 '$outputDirFull' | Out-Null"
        }
    }
}

# -----------------------------------------------------------------------------
# Step: Output

if ($OldIsMacOS) { open "$outputDir" } else { start "$outputDir" }

Exit-Script